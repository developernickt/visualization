import React from "react";
import "./App.css";
import AppShell from "./AppShell/AppShell";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <AppShell />;
    </BrowserRouter>
  );
}

export default App;
