import React, { FC } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import AppNavBar from "../AppNavBar/AppNavBar";
// import Home from "../Home/Home";
import styles from "./AppShell.module.css";
import Landing from "../components/Landing/Landing";
import RunModel from "../components/RunModel/RunModel";

interface AppShellProps {}

const AppShell: FC<AppShellProps> = () => (
  <div className={styles.AppShell}>
    <AppNavBar />
    <div className={styles.AppContent}>
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Landing />} />
        <Route path="/model" element={<RunModel />} />
      </Routes>
    </div>
  </div>
);

export default AppShell;
