import { configureStore } from "@reduxjs/toolkit";
import { runModelReducer } from "./reducers/runModelReducer";

export const store = configureStore({
  reducer: {
    runModel: runModelReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
