interface ParamSetting {
  param: string;
  mean: number;
  deviation: number;
}

interface RunModelSettings {
  settings: ParamSetting[];
}

const initialState = {
  settings: [
    {
      param: "Alpha",
      mean: 0,
      deviation: 0,
    },
    {
      param: "IC",
      mean: 0,
      deviation: 0,
    },
    {
      param: "R",
      mean: 0,
      deviation: 0,
    },
    {
      param: "F",
      mean: 0,
      deviation: 0,
    },
  ],
};

export const UPDATE_PARAM_MEAN = "RUN_MODEL_UPDATE_PARAM_MEAN";
export const UPDATE_PARAM_DEVIATION = "RUN_MODEL_UPDATE_PARAM_DEVIATION";

export const runModelReducer = (
  state: RunModelSettings = initialState,
  action: any
): RunModelSettings => {
  switch (action.type) {
    case UPDATE_PARAM_MEAN: {
      return {
        settings: state.settings.map((param: ParamSetting) => {
          if (param.param === action.param) {
            return {
              ...param,
              mean: action.value,
            };
          } else {
            return param;
          }
        }),
      };
    }
    case UPDATE_PARAM_DEVIATION: {
      return {
        settings: state.settings.map((param: ParamSetting) => {
          if (param.param === action.param) {
            return {
              ...param,
              deviation: action.value,
            };
          } else {
            return param;
          }
        }),
      };
    }
  }
  return state;
};
