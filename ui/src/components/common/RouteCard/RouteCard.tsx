import React, { FC } from "react";
import styles from "./RouteCard.module.css";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";

interface RouteCardProps {
  image: any;
  routeTitle: string;
  route: string;
  routeDescription?: string;
}

const RouteCard: FC<RouteCardProps> = (props) => {
  const navigate = useNavigate();

  const handleCardClick = () => {
    navigate(props.route);
  };

  return (
    <Card
      className={styles.RouteCard}
      sx={{ maxWidth: 345 }}
      onClick={handleCardClick}
    >
      <CardMedia component="img" height="140" image={props.image} alt="" />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.routeTitle}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {props.routeDescription}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default RouteCard;
