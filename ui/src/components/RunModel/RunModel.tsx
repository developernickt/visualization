import React, { FC } from "react";
import styles from "./RunModel.module.css";
import { useSelector, useDispatch } from "react-redux";
import Slider from "@mui/material/Slider/Slider";
import {
  UPDATE_PARAM_DEVIATION,
  UPDATE_PARAM_MEAN,
} from "../../reducers/runModelReducer";
import { Button, Grid, Typography } from "@mui/material";
import axios from "axios";

interface RunModelProps {}

const RunModel: FC<RunModelProps> = () => {
  const dispatch = useDispatch();
  const settings = useSelector((state: any) => state.runModel.settings);

  const handleMeanUpdate = (event: any) => {
    dispatch({
      type: UPDATE_PARAM_MEAN,
      param: event.target.name,
      value: event.target.value,
    });
  };

  const handleDeviationUpdate = (event: any) => {
    dispatch({
      type: UPDATE_PARAM_DEVIATION,
      param: event.target.name,
      value: event.target.value,
    });
  };

  const runModel = async () => {
    try {
      const response = await axios.put(
        "https://9t170ibxah.execute-api.us-east-1.amazonaws.com/Prod/model",
        { settings }
      );
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className={styles.RunModel}>
      <div className={styles.ContentContainer}>
        <div className={styles.Title}>Adjust Model Metrics for Mock Run</div>
        {settings.map((setting: any) => {
          return (
            <Grid container key={setting.param} spacing={2}>
              <Grid item xs={2}>
                {setting.param}
              </Grid>
              <Grid item className={styles.SliderContainer} xs={5}>
                <Typography gutterBottom>Mean</Typography>
                <Slider
                  value={setting.mean}
                  valueLabelDisplay="auto"
                  step={1}
                  marks
                  min={-5}
                  max={5}
                  onChange={handleMeanUpdate}
                  name={setting.param}
                />
              </Grid>
              <Grid item className={styles.SliderContainer} xs={5}>
                <Typography gutterBottom>Deviation</Typography>
                <Slider
                  value={setting.deviation}
                  valueLabelDisplay="auto"
                  step={1}
                  marks
                  min={-5}
                  max={5}
                  onChange={handleDeviationUpdate}
                  name={setting.param}
                />
              </Grid>
            </Grid>
          );
        })}
        <Button variant="contained" onClick={runModel}>
          Run Model
        </Button>
      </div>
    </div>
  );
};

export default RunModel;
