import React, { FC } from "react";
import styles from "./Landing.module.css";
import RouteCard from "../common/RouteCard/RouteCard";

interface LandingProps {}

const Landing: FC<LandingProps> = () => (
  <div className={styles.Landing}>
    <RouteCard
      image={require("../../assets/run_new_model.jpg")}
      routeTitle="Run New Model"
      route="/model"
      routeDescription="Use this page to run a new model though a 'simulation' and generate mocked up data. This can then be reviewed and analyzed under the analysis module."
    />
    <RouteCard
      image={require("../../assets/analysis.jpg")}
      routeTitle="Analysis"
      route="/analysis"
      routeDescription="Use this page to view metrics of model runs, and compare different runs against one another. Acts as a dashboard for analysis."
    />
  </div>
);

export default Landing;
