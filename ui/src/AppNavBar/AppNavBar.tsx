import React, { FC } from "react";
import styles from "./AppNavBar.module.css";
import { IconButton } from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import { useNavigate } from "react-router-dom";

interface AppNavBarProps {}

const AppNavBar: FC<AppNavBarProps> = () => {
  const navigate = useNavigate();

  const navigateToHomePage = () => {
    navigate("/home");
  };

  return (
    <div className={styles.AppBar}>
      <IconButton onClick={navigateToHomePage} size="small">
        <HomeIcon />
      </IconButton>
      <div className={styles.Title}>Model Simulation and Visualization</div>
    </div>
  );
};

export default AppNavBar;
