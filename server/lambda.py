import json

def format_response(body):
    return {
        "statusCode": 200,
        "headers": {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,PUT,GET'
        },
        "body": json.dumps(body)
    }

def create_model_run(event, context):
    request_payload = json.loads(event['body'])
    print(request_payload)
    return format_response({})